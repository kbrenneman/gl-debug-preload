/*
 * Copyright (c) 2021, NVIDIA CORPORATION.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "gl_debug_common.h"

#include <stdio.h>
#include <string.h>

#include "gl_helpers.h"

/**
 * Called when we get an OpenGL error.
 *
 * This function is empty, but it exists as a place to put a breakpoint in the
 * debugger.
 */
static void on_gl_error(void)
{
}

/**
 * Called when we get a high-severity OpenGL message.
 *
 * This function is empty, but it exists as a place to put a breakpoint in the
 * debugger.
 */
static void on_gl_high_severity(void)
{
}

/**
 * Tries to load an OpenGL function, and prints an error message if it fails.
 */
static void *load_gl_function(GetProcAddressFunction getProcAddress, const char *name)
{
    void *ptr = getProcAddress(name);
    if (ptr == NULL)
    {
        printf("WARNING: Can't load GL function %s\n", name);
    }
    return ptr;
}

/**
 * Checks if an extension is supported in a GL or GLES 3.0 or later context.
 *
 * In OpenGL/GLES 3.0 or later, you can use glGetStringi to enumerate extension
 * one at a time, and in an OpenGL core context, that's the only way to do it.
 */
static GLboolean check_extension_gl3(GetProcAddressFunction getProcAddress, const char *name)
{
    void (APIENTRYP ptr_glGetIntegerv) (GLenum pname, GLint *data) =
        load_gl_function(getProcAddress, "glGetIntegerv");
    PFNGLGETSTRINGIPROC ptr_glGetStringi = load_gl_function(getProcAddress, "glGetStringi");
    GLint count = 0;
    GLint i;

    if (ptr_glGetIntegerv == NULL || ptr_glGetStringi == NULL)
    {
        return GL_FALSE;
    }

    ptr_glGetIntegerv(GL_NUM_EXTENSIONS, &count);
    for (i=0; i<count; i++)
    {
        const char *ext = (const char *) ptr_glGetStringi(GL_EXTENSIONS, i);
        if (ext != NULL && strcmp(name, ext) == 0)
        {
            return GL_TRUE;
        }
    }
    return GL_FALSE;
}

/**
 * Checks if an extension is supported by the current context.
 */
static GLboolean check_extension(GetProcAddressFunction getProcAddress, const char *name)
{
    const GLubyte *(APIENTRYP ptr_glGetString) (GLenum name) =
        load_gl_function(getProcAddress, "glGetString");
    const char *version;

    if (ptr_glGetString == NULL)
    {
        return GL_FALSE;
    }

    version = (const char *) ptr_glGetString(GL_VERSION);
    if (check_version_string(version, 3, 0))
    {
        // For an OpenGL or GLES 3.0 or later context, use glGetIntegerv and
        // glGetStringi.
        return check_extension_gl3(getProcAddress, name);
    }
    else
    {
        // Otherwise, we can just call glGetString and search it.
        const char *extensions = (const char *) ptr_glGetString(GL_EXTENSIONS);
        return check_extension_string(extensions, name);
    }
}

static void gl_debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei len, const GLchar *message, const void *param)
{
    const char *sourceStr;
    char sourceBuf[32];
    const char *typeStr;
    char typeBuf[32];
    const char *severityStr;
    char severityBuf[32];

    switch (source)
    {
        case GL_DEBUG_SOURCE_API: sourceStr = "API"; break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM: sourceStr = "WINDOW_SYSTEM"; break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER: sourceStr = "SHADER_COMPILER"; break;
        case GL_DEBUG_SOURCE_THIRD_PARTY: sourceStr = "THIRD_PARTY"; break;
        case GL_DEBUG_SOURCE_APPLICATION: sourceStr = "APPLICATION"; break;
        case GL_DEBUG_SOURCE_OTHER: sourceStr = "OTHER"; break;
        default:
            snprintf(sourceBuf, sizeof(sourceBuf), "source=0x%04x", source);
            sourceStr = sourceBuf;
            break;
    }

    switch (type)
    {
        case GL_DEBUG_TYPE_ERROR: typeStr = "ERROR"; break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: typeStr = "DEPRECATED_BEHAVIOR"; break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: typeStr = "UNDEFINED_BEHAVIOR"; break;
        case GL_DEBUG_TYPE_PORTABILITY: typeStr = "PORTABILITY"; break;
        case GL_DEBUG_TYPE_PERFORMANCE: typeStr = "PERFORMANCE"; break;
        case GL_DEBUG_TYPE_OTHER: typeStr = "OTHER"; break;
        case GL_DEBUG_TYPE_MARKER: typeStr = "MARKER"; break;
        default:
            snprintf(typeBuf, sizeof(typeBuf), "type=0x%04x", type);
            typeStr = typeBuf;
            break;
    }

    switch (severity)
    {
        case GL_DEBUG_SEVERITY_HIGH: severityStr = "HIGH"; break;
        case GL_DEBUG_SEVERITY_MEDIUM: severityStr = "MEDIUM"; break;
        case GL_DEBUG_SEVERITY_LOW: severityStr = "LOW"; break;
        case GL_DEBUG_SEVERITY_NOTIFICATION: severityStr = "NOTIFICATION"; break;
        default:
            snprintf(severityBuf, sizeof(severityBuf), "severity=0x%04x", severity);
            severityStr = severityBuf;
            break;
    }
    printf("GL DEBUG: %s %s %s : 0x%04x %s\n", sourceStr, typeStr, severityStr, id, message);
    fflush(stdout);

    if (type == GL_DEBUG_TYPE_ERROR)
    {
        on_gl_error();
    }
    if (severity == GL_DEBUG_SEVERITY_HIGH)
    {
        on_gl_high_severity();
    }
}

void enable_gl_debug_output(GetProcAddressFunction getProcAddress)
{
    PFNGLDEBUGMESSAGECALLBACKPROC ptr_glDebugMessageCallback = NULL;
    PFNGLDEBUGMESSAGECONTROLPROC ptr_glDebugMessageControl = NULL;
    void (APIENTRYP ptr_glEnable) (GLenum cap);

    printf("Enabling GL debug output\n");

    ptr_glEnable = load_gl_function(getProcAddress, "glEnable");
    if (ptr_glEnable == NULL)
    {
        return;
    }

    // Look up the extension functions. These may or may not have an "ARB"
    // suffix, depending on which extension we find.
    if (check_extension(getProcAddress, "GL_KHR_debug"))
    {
        ptr_glDebugMessageCallback = load_gl_function(getProcAddress, "glDebugMessageCallback");
        ptr_glDebugMessageControl = load_gl_function(getProcAddress, "glDebugMessageControl");
    }
    else if (check_extension(getProcAddress, "GL_ARB_debug_output"))
    {
        ptr_glDebugMessageCallback = load_gl_function(getProcAddress, "glDebugMessageCallbackARB");
        ptr_glDebugMessageControl = load_gl_function(getProcAddress, "glDebugMessageControlARB");
    }
    else
    {
        printf("Current context does not support GL_KHR_debug or GL_ARB_debug_output.\n");
        return;
    }

    if (ptr_glDebugMessageCallback == NULL || ptr_glDebugMessageControl == NULL)
    {
        return;
    }

    // Enable all messages, and make them synchronous.
    ptr_glDebugMessageCallback(gl_debug_callback, NULL);
    ptr_glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
    ptr_glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
}
