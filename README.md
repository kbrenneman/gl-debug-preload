# LD\_PRELOAD libraries for EGL/OpenGL Debugging

This is a pair of libraries that can enable debug messages from EGL and OpenGL.
Both libraries are designed to be loaded using `LD_PRELOAD`, so they can work
with an existing, unmodified application.

`egl_debug_preload.so` uses [`EGL_KHR_debug`](https://www.khronos.org/registry/EGL/extensions/KHR/EGL_KHR_debug.txt)
to get debug messages from EGL. This library simply registers a callback when it loads.

`gl_debug_preload.so` uses [`GL_KHR_debug`](https://www.khronos.org/registry/OpenGL/extensions/KHR/KHR_debug.txt)
to get debug messages from OpenGL. To do that, the library has to intercept
calls to the CreateContext and MakeCurrent calls from EGL and GLX. As a result,
this library will not work with an application that loads libGL.so or libEGL.so
at runtime.

## Using the libraries.

To use these libraries, just set `LD_PRELOAD` to point to them:

```bash
# Enable debug output from both EGL and OpenGL
LD_PRELOAD=/path/to/egl_debug_preload.so:/path/to/gl_debug_preload.so some_egl_application

# If you only need debug output from EGL, then just use one library
LD_PRELOAD=/path/to/egl_debug_preload.so some_egl_application

# Or, if you only need OpenGL output, then this will work with an EGL or GLX
# program.
LD_PRELOAD=/path/to/gl_debug_preload.so some_opengl_application
```

In addition, the libraries have an empty internal function which they call when
they get certain message types. They serve as a place to put a breakpoint in
the debugger:

`on_egl_error()` in `egl_debug_preload.so` is called whenever it receives an
EGL error (`EGL_DEBUG_MSG_ERROR_KHR`) message. It's not called for any warning
or informational messages.

`on_gl_error()` and `on_gl_high_severity()` in `gl_debug_preload.so` are called
for OpenGL error messages and for high-severity messages, respectively.
