/*
 * Copyright (c) 2021, NVIDIA CORPORATION.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * \file
 *
 * A simple library that will enable EGL debug output using EGL_KHR_debug when
 * it loads.
 */

#include <stdio.h>
#include <stdlib.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "gl_helpers.h"

static void on_load(void) __attribute__((constructor));

/**
 * Called when we get an EGL error.
 *
 * This function is empty, but it exists as a place to put a breakpoint in the
 * debugger.
 */
static void on_egl_error(void)
{
}

static void egl_debug_callback(EGLenum error, const char *command, EGLint messageType,
        EGLLabelKHR threadLabel, EGLLabelKHR objectLabel, const char *message)
{
    const char *typeStr;
    char typeBuf[32];

    const char *errorStr;
    char errorBuf[32];

    switch (messageType)
    {
        case EGL_DEBUG_MSG_INFO_KHR: typeStr = " INFO"; break;
        case EGL_DEBUG_MSG_WARN_KHR: typeStr = " WARN"; break;
        case EGL_DEBUG_MSG_ERROR_KHR: typeStr = " ERROR"; break;
        default:
            snprintf(typeBuf, sizeof(typeBuf), " type=0x%04x", messageType);
            typeStr = typeBuf;
            break;
    }

	switch (error)
	{
		case EGL_SUCCESS: errorStr = " EGL_SUCCESS"; break;
		case EGL_NOT_INITIALIZED: errorStr = " EGL_NOT_INITIALIZED"; break;
		case EGL_BAD_ACCESS: errorStr = " EGL_BAD_ACCESS"; break;
		case EGL_BAD_ALLOC: errorStr = " EGL_BAD_ALLOC"; break;
		case EGL_BAD_ATTRIBUTE: errorStr = " EGL_BAD_ATTRIBUTE"; break;
		case EGL_BAD_CONFIG: errorStr = " EGL_BAD_CONFIG"; break;
		case EGL_BAD_CONTEXT: errorStr = " EGL_BAD_CONTEXT"; break;
		case EGL_BAD_CURRENT_SURFACE: errorStr = " EGL_BAD_CURRENT_SURFACE"; break;
		case EGL_BAD_DISPLAY: errorStr = " EGL_BAD_DISPLAY"; break;
		case EGL_BAD_MATCH: errorStr = " EGL_BAD_MATCH"; break;
		case EGL_BAD_NATIVE_PIXMAP: errorStr = " EGL_BAD_NATIVE_PIXMAP"; break;
		case EGL_BAD_NATIVE_WINDOW: errorStr = " EGL_BAD_NATIVE_WINDOW"; break;
		case EGL_BAD_PARAMETER: errorStr = " EGL_BAD_PARAMETER"; break;
		case EGL_BAD_SURFACE: errorStr = " EGL_BAD_SURFACE"; break;
		case EGL_CONTEXT_LOST: errorStr = " EGL_CONTEXT_LOST"; break;
		default:
            snprintf(errorBuf, sizeof(errorBuf), " error=0x%04x", error);
            errorStr = errorBuf;
            break;
	}

    printf("EGL DEBUG: %s %s %s: %s\n", command, typeStr, errorStr, message);
    fflush(stdout);

    if (messageType == EGL_DEBUG_MSG_ERROR_KHR)
    {
        on_egl_error();
    }
}

static EGLBoolean enable_egl_debug_output(void)
{
    static const EGLAttrib DEBUG_ATTRIBS[9] = {
        EGL_DEBUG_MSG_INFO_KHR, EGL_TRUE,
        EGL_DEBUG_MSG_WARN_KHR, EGL_TRUE,
        EGL_DEBUG_MSG_ERROR_KHR, EGL_TRUE,
        EGL_DEBUG_MSG_CRITICAL_KHR, EGL_TRUE,
        EGL_NONE
    };
    PFNEGLDEBUGMESSAGECONTROLKHRPROC ptr_eglDebugMessageControlKHR;
    EGLint err;

    if (!check_extension_string(eglQueryString(EGL_NO_DISPLAY, EGL_EXTENSIONS), "EGL_KHR_debug"))
    {
        printf("Can't enable EGL debug callback: EGL_KHR_debug is not supported\n");
        return EGL_FALSE;
    }

    ptr_eglDebugMessageControlKHR = (PFNEGLDEBUGMESSAGECONTROLKHRPROC) eglGetProcAddress("eglDebugMessageControlKHR");
    if (ptr_eglDebugMessageControlKHR == NULL)
    {
        printf("Can't enable EGL debug callback: Can't load eglDebugMessageControlKHR\n");
        return EGL_FALSE;
    }

    err = ptr_eglDebugMessageControlKHR(egl_debug_callback, DEBUG_ATTRIBS);
    if (err != EGL_SUCCESS)
    {
        printf("Can't enable debug callback: eglDebugMessageControlKHR failed: 0x%x\n", err);
        return EGL_FALSE;
    }

    return EGL_TRUE;
}

void on_load(void)
{
    printf("Initializing EGL_KHR_debug preload library.\n");
    enable_egl_debug_output();
    fflush(stdout);
}
