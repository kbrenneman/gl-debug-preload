/*
 * Copyright (c) 2021, NVIDIA CORPORATION.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <pthread.h>

#include <X11/Xlib.h>
#include <GL/glx.h>
#include <GL/glxext.h>

#include "gl_debug_common.h"

#define PUBLIC __attribute__((visibility("default")))

static int (* ptr_glXGetConfig) (Display *dpy, XVisualInfo *visual, int attrib, int *value);
static GLXFBConfig * (* ptr_glXChooseFBConfig) (Display *dpy, int screen, const int *attribList, int *nitems);
static PFNGLXGETPROCADDRESSPROC ptr_glXGetProcAddress = NULL;
static GLXContext (* ptr_glXCreateContext) (Display *dpy, XVisualInfo *vis, GLXContext shareList, Bool direct) = NULL;
static Bool (* ptr_glXMakeCurrent) (Display *dpy, GLXDrawable drawable, GLXContext ctx) = NULL;
static PFNGLXMAKECONTEXTCURRENTPROC ptr_glXMakeContextCurrent = NULL;
static PFNGLXCREATECONTEXTATTRIBSARBPROC ptr_glXCreateContextAttribsARB = NULL;
static int (* ptr_XFree) (void *data);

static pthread_once_t initOnceControl = PTHREAD_ONCE_INIT;

#define LOAD_PROC(name) loadProcImpl(#name, (__GLXextFuncPtr *) &ptr_##name)
static void loadProcImpl(const char *name, __GLXextFuncPtr *func)
{
    *func = ptr_glXGetProcAddress((const GLubyte *) name);
    if (*func == NULL)
    {
        printf("Can't load %s\n", name);
        fflush(stdout);
        abort();
    }
}

static void initLibraryOnce(void)
{
    printf("Initializing GL_KHR_debug intercept library for GLX.\n");

    ptr_glXGetProcAddress = dlsym(RTLD_NEXT, "glXGetProcAddress");
    if (ptr_glXGetProcAddress == NULL)
    {
        printf("Can't load glXGetProcAddress: %s\n", dlerror());
        fflush(stdout);
        abort();
    }

    LOAD_PROC(glXCreateContext);
    LOAD_PROC(glXGetConfig);
    LOAD_PROC(glXChooseFBConfig);
    LOAD_PROC(glXMakeCurrent);
    LOAD_PROC(glXMakeContextCurrent);
    LOAD_PROC(glXCreateContextAttribsARB);
    LOAD_PROC(XFree);
}

static void initLibrary(void)
{
    pthread_once(&initOnceControl, initLibraryOnce);
}

PUBLIC GLXContext glXCreateContextAttribsARB(Display *dpy, GLXFBConfig config,
        GLXContext share_context, Bool direct, const int *attrib_list)
{
    int *new_attribs = NULL;
    int attrib_count = 0;
    int flags = 0;
    int i;

    initLibrary();

    if (attrib_list != NULL)
    {
        while (attrib_list[attrib_count] != None)
        {
            attrib_count += 2;
        }
    }

    new_attribs = alloca((attrib_count + 3) * sizeof(int));
    attrib_count = 0;
    for (i=0; attrib_list[i] != None; i += 2)
    {
        if (attrib_list[i] == GLX_CONTEXT_FLAGS_ARB)
        {
            flags = attrib_list[i + 1];
        }
        else
        {
            new_attribs[attrib_count++] = attrib_list[i];
            new_attribs[attrib_count++] = attrib_list[i + 1];
        }
    }

    flags |= GLX_CONTEXT_DEBUG_BIT_ARB;
    new_attribs[attrib_count++] = GLX_CONTEXT_FLAGS_ARB;
    new_attribs[attrib_count++] = flags;
    new_attribs[attrib_count++] = None;

    return ptr_glXCreateContextAttribsARB(dpy, config, share_context, direct, new_attribs);
}

PUBLIC GLXContext glXCreateContext(Display *dpy, XVisualInfo *vis, GLXContext share_context, Bool direct)
{
    /*
     * For this, we need to convert the XVisualInfo to a GLXFBConfig.
     * According to the spec (section 3.5):
     *
     *  Calling glXCreateContext(dpy, visual, share list, direct) is equivalent to call-
     *  ing glXCreateNewContext(dpy, config, render type, share list, direct) where
     *  config is the GLXFBConfig identified by the GLX FBCONFIG ID attribute of
     *  visual. If visual’s GLX RGBA attribute is True then render type is taken as
     *  GLX RGBA TYPE , otherwise GLX COLOR INDEX TYPE . The GLXFBConfig iden-
     *  tified by the GLX FBCONFIG ID attribute of visual is associated with the resulting
     *  context.
     *
     * Unfortunately, we can't query GLX_FBCONFIG_ID using glXGetConfig. So
     * instead, look for a GLXFBConfig with a matching visual XID, since
     * there's generally a 1-1 correspondence between visuals and GLXFBConfigs.
     */

    int configAttribs[3] = {
        GLX_VISUAL_ID, vis->visualid,
        None,
    };
    static const int contextAttribs[] = {
        GLX_RENDER_TYPE, GLX_RGBA_TYPE,
        GLX_CONTEXT_FLAGS_ARB, GLX_CONTEXT_DEBUG_BIT_ARB,
        None
    };
    GLXFBConfig *configs = NULL;
    int num = 0;
    GLXContext ctx;

    initLibrary();

    printf("WARNING: Application called glXCreateContext, which may not work.\n");

    configs = ptr_glXChooseFBConfig(dpy, vis->screen, configAttribs, &num);
    if (configs == NULL || num <= 0)
    {
        printf("WARNING: Can't get GLXFBConfig from XVisualInfo\n");
        if (configs != NULL)
        {
            ptr_XFree(configs);
        }
        return ptr_glXCreateContext(dpy, vis, share_context, direct);
    }

    ctx = ptr_glXCreateContextAttribsARB(dpy, configs[0], share_context, direct, contextAttribs);
    ptr_XFree(configs);
    return ctx;
}

PUBLIC GLXContext glXCreateNewContext(Display *dpy, GLXFBConfig config,
        int renderType, GLXContext share_context, Bool direct)
{
    int attribs[] =
    {
        GLX_RENDER_TYPE, renderType,
        GLX_CONTEXT_FLAGS_ARB, GLX_CONTEXT_DEBUG_BIT_ARB,
        None
    };

    initLibrary();

    return ptr_glXCreateContextAttribsARB(dpy, config, share_context, direct, attribs);
}

PUBLIC Bool glXMakeCurrent(Display *dpy, GLXDrawable drawable, GLXContext ctx)
{
    Bool success;

    initLibrary();

    success = ptr_glXMakeCurrent(dpy, drawable, ctx);
    if (success && ctx != NULL)
    {
        enable_gl_debug_output((GetProcAddressFunction) ptr_glXGetProcAddress);
    }
    return success;
}

PUBLIC Bool glXMakeContextCurrent(Display *dpy, GLXDrawable draw, GLXDrawable read, GLXContext ctx)
{
    Bool success;

    initLibrary();

    success = ptr_glXMakeContextCurrent(dpy, draw, read, ctx);
    if (success && ctx != NULL)
    {
        enable_gl_debug_output((GetProcAddressFunction) ptr_glXGetProcAddress);
    }
    return success;
}

PUBLIC __GLXextFuncPtr glXGetProcAddress(const GLubyte *procname)
{
    initLibrary();

    if (strcmp((const char *) procname, "glXGetProcAddress") == 0)
    {
        return (__GLXextFuncPtr) glXGetProcAddress;
    }
    else if (strcmp((const char *) procname, "glXGetProcAddressARB") == 0)
    {
        return (__GLXextFuncPtr) glXGetProcAddress;
    }
    else if (strcmp((const char *) procname, "glXCreateContextAttribsARB") == 0)
    {
        return (__GLXextFuncPtr) glXCreateContextAttribsARB;
    }
    else if (strcmp((const char *) procname, "glXCreateContext") == 0)
    {
        return (__GLXextFuncPtr) glXCreateContext;
    }
    else if (strcmp((const char *) procname, "glXCreateNewContext") == 0)
    {
        return (__GLXextFuncPtr) glXCreateNewContext;
    }
    else if (strcmp((const char *) procname, "glXMakeCurrent") == 0)
    {
        return (__GLXextFuncPtr) glXMakeCurrent;
    }
    else if (strcmp((const char *) procname, "glXMakeContextCurrent") == 0)
    {
        return (__GLXextFuncPtr) glXMakeContextCurrent;
    }
    else
    {
        return (__GLXextFuncPtr) ptr_glXGetProcAddress(procname);
    }
}

