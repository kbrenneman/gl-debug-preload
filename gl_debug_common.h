/*
 * Copyright (c) 2021, NVIDIA CORPORATION.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef GL_DEBUG_COMMON_H
#define GL_DEBUG_COMMON_H

/**
 * \file
 *
 * Common functions for setting up an OpenGL debug callback.
 *
 * These functions handle the OpenGL tasks for setting up a debug callback
 * using GL_KHR_debug or GL_ARB_debug_output. Other than needing the correct
 * egl/glXGetProcAddress callback, none of this depends on EGL or GLX.
 */

#include <GL/gl.h>
#include <GL/glext.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void (* GLProc) (void);
typedef GLProc (* GetProcAddressFunction) (const char *name);

/**
 * Sets up a debug callback for the current context.
 *
 * This is called from eglMakeCurrent or glXMakeCurrent.
 *
 * \param getProcAddress A pointer to either eglGetProcAddress or
 * glXGetProcAddress, depending on which API the current context belongs to.
 */
void enable_gl_debug_output(GetProcAddressFunction getProcAddress);

#ifdef __cplusplus
}
#endif

#endif // GL_DEBUG_COMMON_H
