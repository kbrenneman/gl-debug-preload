/*
 * Copyright (c) 2021, NVIDIA CORPORATION.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _GNU_SOURCE

/**
 * \file
 *
 * Intercept functions to enable debug messages for EGL contexts.
 *
 * These functions will intercept EGL calls to enable the debug flag for
 * contexts, and to set up a debug callback when a context is made current.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <pthread.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "gl_debug_common.h"

#define PUBLIC __attribute__((visibility("default")))

static PFNEGLGETPROCADDRESSPROC ptr_eglGetProcAddress = NULL;
static PFNEGLCREATECONTEXTPROC ptr_eglCreateContext = NULL;
static PFNEGLMAKECURRENTPROC ptr_eglMakeCurrent = NULL;

static pthread_once_t initOnceControl = PTHREAD_ONCE_INIT;

#define LOAD_PROC(name) loadProcImpl(#name, (__eglMustCastToProperFunctionPointerType *) &ptr_##name)
static void loadProcImpl(const char *name, __eglMustCastToProperFunctionPointerType *func)
{
    *func = ptr_eglGetProcAddress(name);
    if (*func == NULL)
    {
        printf("Can't load %s\n", name);
        fflush(stdout);
        abort();
    }
}

static void initLibraryOnce(void)
{
    printf("Initializing GL_KHR_debug intercept library for EGL.\n");

    ptr_eglGetProcAddress = dlsym(RTLD_NEXT, "eglGetProcAddress");
    if (ptr_eglGetProcAddress == NULL)
    {
        printf("Can't load eglGetProcAddress: %s\n", dlerror());
        fflush(stdout);
        abort();
    }

    LOAD_PROC(eglMakeCurrent);
    LOAD_PROC(eglCreateContext);
}

static void initLibrary(void)
{
    pthread_once(&initOnceControl, initLibraryOnce);
}

PUBLIC EGLContext EGLAPIENTRY eglCreateContext(EGLDisplay dpy, EGLConfig config, EGLContext share_context, const EGLint *attrib_list)
{
    EGLint *new_attribs = NULL;
    EGLint attrib_count = 0;
    EGLint flags = 0;
    EGLint i;

    initLibrary();

    // Allocate a new attribute list so that we can add the
    // EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR flag.
    if (attrib_list != NULL)
    {
        while (attrib_list[attrib_count] != EGL_NONE)
        {
            attrib_count += 2;
        }
    }

    new_attribs = alloca((attrib_count + 3) * sizeof(EGLint));
    attrib_count = 0;
    if (attrib_list != NULL)
    {
        for (i=0; attrib_list[i] != EGL_NONE; i += 2)
        {
            if (attrib_list[i] == EGL_CONTEXT_FLAGS_KHR)
            {
                flags = attrib_list[i + 1];
            }
            else
            {
                new_attribs[attrib_count++] = attrib_list[i];
                new_attribs[attrib_count++] = attrib_list[i + 1];
            }
        }
    }

    flags |= EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR;
    new_attribs[attrib_count++] = EGL_CONTEXT_FLAGS_KHR;
    new_attribs[attrib_count++] = flags;
    new_attribs[attrib_count++] = EGL_NONE;

    return ptr_eglCreateContext(dpy, config, share_context, new_attribs);
}

PUBLIC EGLBoolean EGLAPIENTRY eglMakeCurrent(EGLDisplay dpy, EGLSurface draw, EGLSurface read, EGLContext ctx)
{
    EGLBoolean success;

    initLibrary();

    success = ptr_eglMakeCurrent(dpy, draw, read, ctx);
    if (success && ctx != EGL_NO_CONTEXT)
    {
        enable_gl_debug_output(ptr_eglGetProcAddress);
    }
    return success;
}

PUBLIC __eglMustCastToProperFunctionPointerType EGLAPIENTRY eglGetProcAddress (const char *procname)
{
    initLibrary();

    if (strcmp(procname, "eglGetProcAddress") == 0)
    {
        return (__eglMustCastToProperFunctionPointerType) eglGetProcAddress;
    }
    else if (strcmp(procname, "eglCreateContext") == 0)
    {
        return (__eglMustCastToProperFunctionPointerType) eglCreateContext;
    }
    else if (strcmp(procname, "eglMakeCurrent") == 0)
    {
        return (__eglMustCastToProperFunctionPointerType) eglMakeCurrent;
    }
    else
    {
        return ptr_eglGetProcAddress(procname);
    }
}
