/*
 * Copyright (c) 2021, NVIDIA CORPORATION.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "gl_helpers.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

// TODO: Move this to a common file
GLboolean check_extension_string(const char *extensionString, const char *name)
{
    const char *ptr = extensionString;
    size_t namelen;

    if (extensionString == NULL) 
    {
        return GL_FALSE;
    }

    namelen = strlen(name);
    if (namelen == 0)
    {
        return GL_FALSE;
    }
    while (1)
    {
        size_t len;

        // Skip leading whitespace.
        while (*ptr == ' ')
        {
            ptr++;
        }
        if (*ptr == '\x00')
        {
            break;
        }
        len = strcspn(ptr, " ");
        assert(len > 0);

        if (len == namelen && strncmp(name, ptr, namelen) == 0)
        {
            return GL_TRUE;
        }

        ptr += len;
    }
    return GL_FALSE;
}

GLboolean parse_version_string(const char *versionString, int *major, int *minor, const char **suffix)
{
    // GLES contexts add a prefix to the GL_VERSION and GL_SHADING_LANGUAGE_VERSION
    // strings. We need to skip that prefix to parse the version number.
    static const char *PREFIXES[] = {
        "OpenGL ES GLSL ES ",
        "OpenGL ES ",
        NULL
    };

    if (versionString != NULL)
    {
        int gotMajor = 0;
        int gotMinor = 0;
        int len = 0;
        int i, ret;

        for (i=0; PREFIXES[i] != NULL; i++)
        {
            size_t prefixLen = strlen(PREFIXES[i]);
            if (strncmp(versionString, PREFIXES[i], prefixLen) == 0)
            {
                versionString += prefixLen;
                break;
            }
        }

        ret = sscanf(versionString, "%d.%d%n", &gotMajor, &gotMinor, &len);
        if (ret == 2)
        {
            if (major != NULL)
            {
                *major = gotMajor;
            }
            if (minor != NULL)
            {
                *minor = gotMinor;
            }
            if (suffix != NULL)
            {
                // Skip any whitespace, and then return whatever else is after
                // the version number.
                const char *ptr = versionString + len;
                while (isspace(*ptr))
                {
                    ptr++;
                }
                *suffix = ptr;
            }
            return GL_TRUE;
        }
    }
    if (major != NULL)
    {
        *major = 0;
    }
    if (minor != NULL)
    {
        *minor = 0;
    }
    if (suffix != NULL)
    {
        *suffix = NULL;
    }
    return GL_FALSE;
}

GLboolean check_version_string(const char *versionString, int major, int minor)
{
    int gotMajor, gotMinor;
    if (parse_version_string(versionString, &gotMajor, &gotMinor, NULL))
    {
        if (gotMajor > major || (gotMajor == major && gotMinor >= minor))
        {
            return GL_TRUE;
        }
    }

    return GL_FALSE;
}
